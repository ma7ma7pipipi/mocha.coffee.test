# README #

coffeescript (javascript) のmochaのテスト雛形

# 必要なインストール #
-gだと何故か通らないので。


```
//srcフォルダ内で
npm install request
npm install cheelio-httpcli

//testフォルダ内で
npm install mocha

```


また、coffeescriptがうまくjsになっていない時があるので注意。
テストの結果がpass 0 の場合は、srcフォルダ内でテストしている。

テスト実行
```
mocha --reporter spec test.js
```

# composer.json #



```
#!json
{
"repositories": [
    {
     "type": "vcs",
     "url": "https://bitbucket.org/ma7ma7pipipi/mocha.coffee.test"
    }
],
"require": {
    "okws/mocha.coffee.test": "dev-master"
}
}

```