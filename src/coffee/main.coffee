request = require('request')
client = require('cheerio-httpcli')
sub = require("../../src/js/sub")

##@マークは必須。これがないと外部から require した時に new できない
class @Person extends sub.Tester
  constructor: (@name) ->
    this.name = @name
  talk: ->
    return this.name

  ajax: (url,callback) ->

    client.fetch url,
      q: "any"
    , (err, $, res) ->
      callback $("title").text()
      return

    return